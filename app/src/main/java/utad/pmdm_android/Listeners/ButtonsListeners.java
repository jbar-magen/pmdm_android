package utad.pmdm_android.Listeners;

import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import utad.pmdm_android.ActivityCentral;
import utad.pmdm_android.MainActivity;
import utad.pmdm_android.R;


/**
 * Created by Yony on 30/09/15.
 */
public class ButtonsListeners implements View.OnClickListener {

    MainActivity MA=null;
    String sLogVTAG="ButtonsListeners";

    public ButtonsListeners(MainActivity MAL){
        this.MA=MAL;
    }

    @Override
    public void onClick(View v) {
        if(v.getId()== R.id.button_ok){
            Toast.makeText(MA.getApplicationContext(), "PRESSED OK", Toast.LENGTH_LONG).show();
            Log.v(sLogVTAG, "" + MA.getEdtxt_nombre().getText());
            Log.v(sLogVTAG, "" + MA.getEdtxt_apellidos().getText());
            Log.v(sLogVTAG, "" + MA.getEdtxt_edad().getText());
            Log.v(sLogVTAG, "" + MA.getEdtxt_email().getText());
            Log.v(sLogVTAG, "" + MA.getEdtxt_password().getText());
        }
        else if(v.getId()== R.id.button_cancel){
            Toast.makeText(MA.getApplicationContext(), "PRESSED CANCEL", Toast.LENGTH_LONG).show();
            MA.getEdtxt_nombre().setText("");
            MA.getEdtxt_apellidos().setText("");
            MA.getEdtxt_edad().setText("");
            MA.getEdtxt_email().setText("");
            MA.getEdtxt_password().setText("");
            MA.linlay_registro.setVisibility(View.GONE);
            MA.linlay_login.setVisibility(View.VISIBLE);
        }
        else if(v.getId()== R.id.btn_registro){
            MA.linlay_registro.setVisibility(View.VISIBLE);
            MA.linlay_login.setVisibility(View.GONE);
        }
        else if(v.getId()== R.id.btn_login){
            Intent intent = new Intent(MA, ActivityCentral.class);
            intent.putExtra("VARIABLE_NOMBRE", "YONY!!!!!");
            intent.putExtra("VARIABLE_APELLIDOS", "B!!!!!");
            MA.startActivity(intent);
        }
    }
}
